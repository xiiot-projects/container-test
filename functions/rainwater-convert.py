# environment: python-env
# language: python

# Copyright (c) 2018 Nutanix, Inc.
# Use of this source code is governed by an MIT-style license 
# found in the LICENSE file at https://github.com/nutanix/xi-iot.

import logging
import json

logging.info("Temperature Convert V1")

'''
Example payload
   payload = '{
    "deviceId": "D13",
    "airt": 85,
    "unit": "F" }'
'''
def main(ctx,msg):
    payload = json.loads(msg)
    logging.info (payload)
    if payload["unit"]=="F":
        logging.info("Converting")
        payload["unit"]="C"
        payload["airt"]=round((int(payload["airt"])-32)*(5/9))
        logging.info(payload["airt"])
    ctx.send(json.dumps(payload).encode())
    return
